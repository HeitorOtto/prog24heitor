from flask import Flask, request, render_template, request

app = Flask(__name__)
@app.route('/')
def hello_world():
    return 'Hey VSauce Michael here, is your home security great, or is it ?'
@app.route('/raiz_eq2')
def rzeq2():
    a = int(request.args.get('n1'))
    b = int(request.args.get('n2'))
    c = int(request.args.get('n3'))

    de = b*b - 4*a*c

    if de < 0:
        return "nao tem raiz real."
    elif de == 0:
        return "uma raiz real, igual a."
    else:
        return "2 raizes"

@app.route("/form")
def form():
    return render_template("form.html")


@app.route('/ha')
def ha():
    a = int(request.args.get('n1'))
    b = int(request.args.get('n2'))
    c = int(request.args.get('n3'))

    de = b*b - 4*a*c

    if de < 0:
        return render_template("ha.html" , resp = "nenhuma raiz")
    elif de == 0:
        return render_template("ha.html" , resp = "uma raiz real")
    else:
        return render_template("ha.html", resp = "2 raizes")
