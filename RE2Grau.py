import math

a = int(input("qual o valor de A: "))
b = int(input("qual o valor de B: "))
c = int(input("qual o valor de C: "))

de = b*b - 4*a*c

# se delta < 0:
    # não tem RR
# senao
    # se delta = 0
        # 1 raiz real, igual a -b/2a
    # senao
        # 1 raizes
        # calcula raiz de delta
        # x1 = (-b+rd)/2a
        # x2 = (-b-rd)/2a

if de < 0:
    print("nao tem raiz real.")
elif de == 0:
    print (f"uma raiz real, igual a {-b/(2*a)}.")
else:
    de1 = math.sqrt(de)
    x1 = (-b+de1)/2*a
    x2 = (-b-de1)/2*a
    print(f"2 raizes,{x1}e{x2}.")
